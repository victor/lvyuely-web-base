package com.lvyuely.web.module.service;

import java.io.Serializable;
import java.util.List;

public interface BaseService<K extends Serializable, V> {

	public boolean save(V vo);

	public boolean deleteById(K id);

	public boolean update(V vo);

	public V getById(K id);

	public List<V> listAll();

	public List<V> listByPage(long start, int length);

}
