package com.lvyuely.web.module.service.func.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lvyuely.web.module.entity.function.JobTimeExpression;
import com.lvyuely.web.module.mapper.func.JobTimeExpressionMapper;
import com.lvyuely.web.module.service.func.JobTimeExpressionService;

@Service
public class JobTimeExpressionServiceImpl implements JobTimeExpressionService {

	private final static Logger logger = Logger.getLogger(JobTimeExpressionServiceImpl.class);

	@Autowired
	private JobTimeExpressionMapper mapper;

	@Override
	public boolean save(JobTimeExpression vo) {
		try {
			return mapper.save(vo) > 0;
		} catch (Throwable t) {
			logger.error("", t);
		}
		return false;
	}

	@Override
	public boolean deleteById(Long id) {
		try {
			return mapper.deleteById(id) > 0;
		} catch (Throwable t) {
			logger.error("", t);
		}
		return false;
	}

	@Override
	public boolean update(JobTimeExpression vo) {
		try {
			return mapper.update(vo) > 0;
		} catch (Throwable t) {
			logger.error("", t);
		}
		return false;
	}

	@Override
	public JobTimeExpression getById(Long id) {
		try {
			return mapper.getById(id);
		} catch (Throwable t) {
			logger.error("", t);
		}
		return null;
	}

	@Override
	public List<JobTimeExpression> listAll() {
		try {
			return mapper.listAll();
		} catch (Throwable t) {
			logger.error("", t);
		}
		return null;
	}

	@Override
	public List<JobTimeExpression> listByPage(long start, int length) {
		try {
			return mapper.listByPage(start, length);
		} catch (Throwable t) {
			logger.error("", t);
		}
		return null;
	}

}
