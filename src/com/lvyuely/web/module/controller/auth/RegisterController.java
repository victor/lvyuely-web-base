package com.lvyuely.web.module.controller.auth;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.lvyuely.web.module.entity.auth.Account;
import com.lvyuely.web.module.service.auth.AccountService;
import com.lvyuely.web.module.util.Encript;

@Controller
@RequestMapping("/auth")
public class RegisterController {

	private final Logger logger = Logger.getLogger(RegisterController.class);
	@Autowired
	private AccountService accountService;

	@RequestMapping(value = "register", method = RequestMethod.GET)
	public String register() {
		return "auth/register/register.ftl";
	}

	@RequestMapping(value = "register", method = RequestMethod.POST)
	public String register(Account account) {
		try {
			account.setPassword(Encript.md5(account.getUsername() + Encript.md5(account.getPassword())));
			if (!accountService.save(account)) {
			}
		} catch (Throwable t) {
			logger.error("", t);
		}
		return "redirect:http://localhost:8080/auth/login";
	}

}
