package com.lvyuely.web.module.mapper.auth;

import com.lvyuely.web.module.entity.auth.Account;
import com.lvyuely.web.module.mapper.BaseMapper;

public interface AccountMapper extends BaseMapper<Long, Account> {

	public Account login(Account account);

}
