package com.lvyuely.web.module.mapper.func;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.lvyuely.web.module.entity.function.JobTimeExpression;
import com.lvyuely.web.module.mapper.BaseMapper;

public interface JobTimeExpressionMapper extends BaseMapper<Long, JobTimeExpression> {

	@Insert("insert into job_time_expression(`trigger`,`name`,`expression`) values('${vo.trigger}','${vo.name}','${vo.expression}')")
	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "vo.id")
	public int save(@Param("vo") JobTimeExpression vo);

	@Delete("delete from job_time_expression where `id` = ${id}")
	public int deleteById(@Param("id") Long id);

	@Update("update job_time_expression set `trigger` = '${vo.trigger}',`name` = '${vo.name}',`expression` ='${vo.expression}' where `id` = ${vo.id}")
	public int update(@Param("vo") JobTimeExpression vo);

	@Select("select `id`,`trigger`,`name`,`expression` from job_time_expression where `id` = ${id}")
	public JobTimeExpression getById(@Param("id") Long id);

	@Select("select `id`,`trigger`,`name`,`expression` from job_time_expression")
	public List<JobTimeExpression> listAll();

	@Select("select `id`,`trigger`,`name`,`expression` from job_time_expression limit ${start},${length}")
	public List<JobTimeExpression> listByPage(@Param("start") long start, @Param("length") int length);

}
