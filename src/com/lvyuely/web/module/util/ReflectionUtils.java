package com.lvyuely.web.module.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionUtils {
	 /**
     * Description: 新建实例  
     * @Version1.0 2009-8-2 下午09:02:04 Tylar（wuliang@sohu-inc.com）created
     * @param className 类名
     * @param args 构造函数的参数 
     * @return 新建的实例
     * @throws Exception
     */
    public static Object newInstance(String className, Object[] args) throws Exception {    
        Class<?> newoneClass = Class.forName(className);    
        Class<?>[] argsClass = new Class[args.length];    
        for (int i = 0, j = args.length; i < j; i++) {    
            argsClass[i] = args[i].getClass();    
        }    
        Constructor<?> cons = newoneClass.getConstructor(argsClass);    
        return cons.newInstance(args);    
    }
    
    /**
     * Description: 新建实例
     * @Version1.0 2009-8-2 下午09:01:07 Tylar（wuliang@sohu-inc.com）created
     * @param className 类名
     * @return 新建的实例
     * @throws ClassNotFoundException 
     * @throws SecurityException 
     * @throws NoSuchMethodException 
     * @throws InvocationTargetException 
     * @throws IllegalArgumentException 
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     * @throws Exception
     */
    public static Object newInstance(String className) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {    
        Class<?> newoneClass = Class.forName(className);    
        Constructor<?> cons = newoneClass.getConstructor();    
        return cons.newInstance();    
    }

    /**
     * Description: 执行某对象方法
     * @Version1.0 2009-8-2 下午08:59:20 Tylar（wuliang@sohu-inc.com）created
     * @param owner 对象
     * @param methodName 方法名 
     * @param args 参数  
     * @return 方法返回值  
     * @throws Exception
     */
    public static Object invokeMethod(Object owner, String methodName, Object[] args)    
            throws Exception {    
        Class<?> ownerClass = owner.getClass();    
        Class<?>[] argsClass = new Class[args.length];    
        for (int i = 0, j = args.length; i < j; i++) {    
            argsClass[i] = args[i].getClass();    
        }    
        Method method = ownerClass.getMethod(methodName, argsClass);    
        return method.invoke(owner, args);    
    }
    
    /**
     * Description: 执行某对象方法
     * @Version1.0 2009-8-2 下午08:59:20 Tylar（wuliang@sohu-inc.com）created
     * @param owner 对象
     * @param methodName 方法名 
     * @param args 参数  
     * @return 方法返回值  
     * @throws Exception
     */
    public static Object invokeMethod(Object owner, String methodName, Object[] args,Class<?>[] classType)    
            throws Exception {    
        Class<?> ownerClass = owner.getClass();    
        Method method = ownerClass.getMethod(methodName, classType);    
        return method.invoke(owner, args);    
    }
    
    /**
     * Description: 执行某对象方法
     * @Version1.0 2009-8-2 下午09:00:25 Tylar（wuliang@sohu-inc.com）created
     * @param owner 对象 
     * @param methodName 方法名
     * @return 方法返回值
     * @throws Exception
     */
    public static Object invokeMethod(Object owner, String methodName)    
            throws Exception {  
        Class<?> ownerClass = owner.getClass();    
        Method method = ownerClass.getMethod(methodName);    
        return method.invoke(owner);
    }

}
