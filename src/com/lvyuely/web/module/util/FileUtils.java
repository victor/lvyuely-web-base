package com.lvyuely.web.module.util;

import java.io.File;

import org.apache.log4j.Logger;

public class FileUtils {

	private final static Logger logger = Logger.getLogger(FileUtils.class);

	/**
	 * 文件删除
	 * 
	 * @param path
	 * @return
	 */
	public static boolean delete(String path) {
		try {
			return delete(new File(path));
		} catch (Throwable t) {
			logger.error("");
		}
		return false;
	}

	/**
	 * 文件删除
	 * 
	 * @param path
	 * @return
	 */
	public static boolean delete(File f) {
		try {
			if (f.exists()) {
				if (f.isFile()) {
					return deleteFile(f);
				} else {
					return deleteFolder(f);
				}
			}
			return true;
		} catch (Throwable t) {
			logger.error("");
		}
		return false;
	}

	private static boolean deleteFile(File f) {
		try {
			if (f.exists()) {
				return f.delete();
			}
			return true;
		} catch (Throwable t) {
			logger.error("");
		}
		return false;
	}

	private static boolean deleteFolder(File f) {
		try {
			if (f.exists()) {
				File[] children = f.listFiles();
				for (int i = 0; children != null && i < children.length; i++) {
					File child = children[i];
					if (!delete(child)) {
						return false;
					}
				}
				return f.delete();
			}
			return true;
		} catch (Throwable t) {
			logger.error("");
		}
		return false;
	}
}
