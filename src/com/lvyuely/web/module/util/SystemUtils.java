package com.lvyuely.web.module.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

public class SystemUtils {
	private static final Logger logger = Logger.getLogger(SystemUtils.class);

	public static Set<String> getNetInterfaceAddress() {
		Set<String> ads = null;
		try {
			ads = new HashSet<String>();
			for (Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces(); interfaces.hasMoreElements();) {
				NetworkInterface networkInterface = interfaces.nextElement();
				if (networkInterface.isLoopback() || !networkInterface.isUp()) {
					continue;
				}
				Enumeration<InetAddress> addresses = networkInterface.getInetAddresses();
				if (addresses.hasMoreElements()) {
					String ip = addresses.nextElement().getHostAddress();
					logger.info("Sever local IP:" + ip);
					ads.add(ip);
				}
			}
		} catch (SocketException e) {
			logger.debug("Error when getting host ip address: <{}>.", e);
		}
		return ads;
	}
}
